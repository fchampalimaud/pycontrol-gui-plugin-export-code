#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('pycontrolgui_plugin_export_code/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Cannot find version information')


setup(
    name='pycontrol-gui-plugin-export-code',
    version=version,
    description="""Plugin to generate a project api code to python""",
    author='Ricardo Jorge Vieira Ribeiro',
    author_email='ricardojvr@gmail.com',
    license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
    url='https://bitbucket.org/fchampalimaud/pycontrol-gui-plugin-export-code',

    include_package_data=True,
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),
    package_data={'pycontrolgui_plugin_export_code': [
            'resources/*.*',
        ]
    }, 
)
