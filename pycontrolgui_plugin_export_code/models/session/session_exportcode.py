# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
from pycontrolgui_plugin_export_code.models.base_exportcode import BaseExportCode

class SessionExportCode(BaseExportCode):
	
	def build_code(self, imports=[], lines=[]):		
		lines.append('{1} = {0}.create_session()'.format(self.setup.variable_name, self.variable_name))
		

		lines.append('{0}.name = "{1}"'.format(self.variable_name, self.name))
		lines.append('{0}.path = "{1}"'.format(self.variable_name, self.path))
		lines.append('{0}.board_name = "{1}"'.format(self.variable_name, self.board_name))
		lines.append('{0}.task_name = "{1}"'.format(self.variable_name, self.task_name))
		lines.append('{0}.board_serial_port = "{1}"'.format(self.variable_name, self.board_serial_port))
		lines.append('{0}.started = "{1}"'.format(self.variable_name, self.started))
		lines.append('{0}.ended = "{1}"'.format(self.variable_name, self.ended))

		lines.append('')


	@property
	def variable_name(self):
		name = BaseExportCode.variable_name.fget(self)
		return 's_{0}'.format(name)