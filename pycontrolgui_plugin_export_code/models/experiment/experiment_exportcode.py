# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
from pycontrolgui_plugin_export_code.models.base_exportcode import BaseExportCode

class ExperimentExportCode(BaseExportCode):
	
	def build_code(self, imports=[], lines=[]):		
		lines.append('{1} = {0}.create_experiment()'.format(self.project.variable_name, self.variable_name))
		
		lines.append('{0}.name = "{1}"'.format(self.variable_name, self.name))
		lines.append('{0}.path = "{1}"'.format(self.variable_name, self.path))
		lines.append('{0}.task = {1}'.format(self.variable_name, self.task.variable_name if self.task else 'None'))

		lines.append('')
		lines.append('# #### SETUPS ########################')
		lines.append('')
		
		for setup in self.setups: setup.build_code(imports, lines)