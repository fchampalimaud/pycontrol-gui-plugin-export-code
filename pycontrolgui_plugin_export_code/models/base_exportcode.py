# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf

class BaseExportCode(object):
	
	@property
	def variable_name(self):
		name = self.name.lower().replace(' ', '_').replace(':', '').replace('-', '_').replace('.', '_')
		return name
	