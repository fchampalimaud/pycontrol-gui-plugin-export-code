# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
from pycontrolgui_plugin_export_code.models.base_exportcode import BaseExportCode
from pycontrolgui_plugin_export_code.exportcode_window import ExportCodeWindow

class ProjectTreeNode(BaseExportCode):
	
	def create_treenode(self, tree):
		node = super(ProjectTreeNode, self).create_treenode(tree)
		tree.addPopupMenuOption('Export code', self.__open_export_code_plugin, item=node, icon=conf.EXPORTCODE_PLUGIN_ICON)
		return node

	def __open_export_code_plugin(self):
		if not hasattr(self, 'export_code_plugin'):
			self.export_code_plugin = ExportCodeWindow(self)
			self.export_code_plugin.show()			
			self.export_code_plugin.subwindow.resize(*conf.EXPORTCODE_PLUGIN_WINDOW_SIZE)         
		else:
			self.export_code_plugin.show()