# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
from pycontrolgui_plugin_export_code.models.project.project_treenode import ProjectTreeNode

class ProjectExportCode(ProjectTreeNode):
	
	def build_code(self, imports=[], lines=[]):		
		imports.append('from pycontrolapi.project import Project')
		
		lines.append('')
		
		lines.append('{0} = Project()'.format(self.variable_name) )

		lines.append('{0}.name = "{1}"'.format(self.variable_name, self.name))
		lines.append('{0}.path = "{1}"'.format(self.variable_name, self.path))

		lines.append('')
		lines.append('# #### BOARDS ########################')
		lines.append('')
		for board 	in self.boards: 		board.build_code(imports, lines)
		lines.append('')
		lines.append('# #### TASKS ########################')
		lines.append('')
		for task 	in self.tasks: 			task.build_code(imports, lines)
		lines.append('')
		lines.append('# #### EXPERIMENTS ########################')
		lines.append('')
		for exp 	in self.experiments: 	exp.build_code(imports, lines)
		lines.append('')



	def export_code(self):
		imports = []
		lines 	= []
		self.build_code(imports, lines)

		lines.append('')
		lines.append('#{0}.save()'.format(self.variable_name))
		lines.append('#{0}.load("{1}")'.format(self.variable_name,self.path))
		
		return '\n'.join(imports) + '\n' +'\n'.join(lines)


