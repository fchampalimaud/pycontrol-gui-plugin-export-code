# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
from pycontrolgui_plugin_export_code.models.base_exportcode import BaseExportCode

class SetupExportCode(BaseExportCode):
	
	def build_code(self, imports=[], lines=[]):		
		lines.append('{1} = {0}.create_setup()'.format(self.experiment.variable_name, self.variable_name))
		
		lines.append('{0}.name = "{1}"'.format(self.variable_name, self.name))
		lines.append('{0}.path = "{1}"'.format(self.variable_name, self.path))
		lines.append('{0}.board = {1}'.format(self.variable_name, self.board.variable_name if self.board else 'None'))

		lines.append('')
		lines.append('# #### {0} SESSIONS ########################'.format(self.name))
		lines.append('')

		for session in self.sessions: session.build_code(imports, lines)
