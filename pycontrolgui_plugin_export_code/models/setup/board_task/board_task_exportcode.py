# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
from pycontrolgui_plugin_export_code.models.base_exportcode import BaseExportCode

class BoardTaskExportCode(BaseExportCode):
	
	def build_code(self, imports=[], lines=[]):		
		lines.append('{1} = {0}.create_board()'.format(self.project.variable_name, self.variable_name))
		
