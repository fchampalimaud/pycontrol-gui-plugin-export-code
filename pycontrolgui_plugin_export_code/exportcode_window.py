#!/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontroltrialsplot.trials_plot"""

import logging

from pyforms.Controls import ControlCodeEditor
from pyforms.Controls import ControlButton
from pyforms import BaseWidget

from pysettings import conf

logger = logging.getLogger(__name__)

class ExportCodeWindow(BaseWidget):
	""" Show all boxes live state for an experiment"""
	
	def __init__(self, project):
		"""
		:param session: session reference
		:type session: pycontrolgui.windows.detail.entities.session_window.SessionWindow
		"""
		BaseWidget.__init__(self, project.name)
		self.project = project
		self._code = ControlCodeEditor('Code editor')
		self._gen = ControlButton('Generate code')

		self._formset = ['_gen','_code']

		self._code.value = self.project.export_code()
		self._gen.value = self.__generate_code_evt

	def __generate_code_evt(self):
		self._code.value = self.project.export_code()

	def show(self):
		# Prevent the call to be recursive because of the mdi_area
		if hasattr(self, '_show_called'):
			BaseWidget.show(self)
			return
		self._show_called = True
		self.mainwindow.mdi_area += self
		del self._show_called

		

	def beforeClose(self): return False

	@property
	def mainwindow(self): return self.project.mainwindow
	

	@property
	def title(self): return BaseWidget.title.fget(self)
	@title.setter
	def title(self, value):
		title = 'Export code: {0}'.format(value)
		BaseWidget.title.fset(self, title)
