import os

EXPORTCODE_PLUGIN_ICON = os.path.join(os.path.dirname(__file__), 'resources', 'code.png')

EXPORTCODE_PLUGIN_WINDOW_SIZE = 1000, 800